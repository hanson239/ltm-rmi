/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.controller;


import Server.controller.DAO.StudentDAO;
import Server.controller.Interface.IController;
import Server.model.Student;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/**
 *
 * @author My PC
 */
public class Controller extends UnicastRemoteObject implements IController{

    public Controller() throws RemoteException {
        super();
    }
    
    StudentDAO dao = new StudentDAO();

    @Override
    public void addStudent(Student student) {
        
        dao.insertStudent(student);
    }

    @Override
    public void editStudent(Student student) {
        dao.updateStudent(student.getId(), student);
    }

    @Override
    public void deleteStudent(int idStudent) {
        System.out.println("Xoá sinh viên id: " + idStudent);
        dao.deleteStudent(idStudent);
    }

    @Override
    public Student findStudentById(int id) {
        
        return dao.findByID(id);
    }

    @Override
    public ArrayList<Student> getAllStudent() {
        return dao.findAllStudent();
    }
    
}
