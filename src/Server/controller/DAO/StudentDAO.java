/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.controller.DAO;

import Server.model.Student;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author My PC
 */
public class StudentDAO extends DAO{
    public StudentDAO() {
        super();
    }
    
//______________________________________________________________________________    
    
    public Student findByID(int id){
        Student student = null;
        try {
            //Tạo truy vấn
            
            Statement statement = conn.createStatement();
            String SQL = "SELECT * from student WHERE student.id=" + id;
            ResultSet resultSet = statement.executeQuery(SQL);
            // ghi kết quả
            System.out.println(SQL);
            if (resultSet.next()){
                student.setId(id);
                student.setHoten(resultSet.getNString("Hoten"));
                student.setNgaysinh(resultSet.getNString("Ngaysinh"));
                student.setLop(resultSet.getNString("Lop"));
            }

        } catch (SQLException ex) {
            System.out.println("Tìm lỗi.");
            return null;
        }
        System.out.println("Tìm sinh viên.");
        return student;
    }
        
    public ArrayList<Student> findAllStudent(){
        ArrayList<Student> res = new ArrayList<>();
        try {
            //Tạo truy vấn

            Statement statement = conn.createStatement();
            String SQL = "SELECT * from student";
            ResultSet resultSet = statement.executeQuery(SQL);

            while (resultSet.next()){
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setHoten(resultSet.getNString("Hoten"));
                student.setNgaysinh(resultSet.getNString("Ngaysinh"));
                student.setLop(resultSet.getNString("Lop"));

                res.add(student);
            }
            System.out.println("Tìm được " + res.size() +" sinh viên.");
        } catch (SQLException ex) {
            System.out.println("Tìm lỗi.");
            return null;
        }

        return res;
    }
   
    
    public void updateStudent(int id, Student student){
        
        try {
            Statement statement = conn.createStatement();
            
            String SQL = "UPDATE student SET " +
                "student.Hoten = '" + student.getHoten() +"'," +
                "student.NgaySinh = '" + student.getNgaysinh() + "'," +
                "student.Lop = '" + student.getLop() + "' " +
                "WHERE student.id = " + student.getId();
            
            int rowCount = statement.executeUpdate(SQL);
            System.out.println("Đã update " + rowCount + " hàng.");
        } catch (SQLException ex) {
            System.out.println("Update lỗi.");
        }
    } 
    
    public void insertStudent(Student student){
        
        try {
            //Tạo truy vấn          
            Statement statement = conn.createStatement();

            // chèn vào bảng baohiem
            String SQL = "Insert Into student Values(0,"
                    + "'" + student.getHoten() + "',"
                    + "'" + student.getNgaysinh()+ "',"
                    + "'" + student.getLop() + "')";

            int rowCount = statement.executeUpdate(SQL);
            System.out.println("Đã thêm " + rowCount + " hàng.");
        } catch (SQLException ex) {
            System.out.println("Thêm bị lỗi");
        }
        
    }
    
    public void deleteStudent(int id){
        try {
            //Tạo truy vấn

            Statement statement = conn.createStatement();
            String SQL = "DELETE from student WHERE student.id= " + id;

            int rowCount = statement.executeUpdate(SQL);
            
            System.out.println("Deleted " + rowCount + " hàng.");

        } catch (SQLException ex) {
            System.out.println("Xoá bị lỗi.");
        }
    }
   
    
}
