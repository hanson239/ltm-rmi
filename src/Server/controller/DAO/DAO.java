/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.controller.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAO {
    public Connection conn;
   
    public DAO(){
        if (conn == null){
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                
                String connectionURL = "jdbc:mysql://localhost:3306/rmidb?allowPublicKeyRetrieval=true&useSSL=false";
                conn = DriverManager.getConnection(connectionURL, "root",
                "rootroot");
                
                
            } catch (ClassNotFoundException|SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}