/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.controller.Interface;

import Server.model.Student;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author My PC
 */
public interface IController extends Remote{
    
    public void addStudent(Student student) throws RemoteException;
    
    public void editStudent(Student student) throws RemoteException;
    
    public void deleteStudent(int intStudent) throws RemoteException;
    
    public Student findStudentById(int id) throws RemoteException;
    
    public ArrayList<Student> getAllStudent() throws RemoteException;
    
    
}
